import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      screens: {
        'xs': '375px',
        // Add more custom breakpoints as needed
      },
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      fontFamily  :{
        gilroy400 : ['var(--font-gilroy-400)'],
        gilroy500 : ['var(--font-gilroy-500)'],
        gilroy600 : ['var(--font-gilroy-600)'],
        gilroy700 : ['var(--font-gilroy-700)'],
        gilroy800 : ['var(--font-gilroy-800)'],
        gilroy900 : ['var(--font-gilroy-900)'],
      }
    },
  },
  plugins: [],
};
export default config;
