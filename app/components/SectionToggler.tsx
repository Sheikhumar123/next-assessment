import { useState } from "react"

const SectionToggler = () => {
    const [section, setSection] = useState(1)
    return (
        <div className="about-background w-[90%] sm:w-[80%] lg:w-[600px] min-h-[471px] rounded-3xl relative top-[60px] lg:right-[61px] z-10" >

            <div className="w-[335px] sm:w-[450px]  md:w-[100%] absolute left-[2%] lg:left-[5%] -top-[60px] h-[60px] flex flex-row justify-between sm:justify-evenly text-[12px] sm:text-[14px] font-gilroy800 leading-[12px] ">

                <button onClick={() => setSection(1)} className={` transition-all  duration-300 bg-transparent border-none ${section == 1 ? 'text-[#ECF3FF] bottom-line' : 'text-[#8690A0]'} `}>
                    Project Owners
                </button>

                <button onClick={() => setSection(2)} className={` transition-all  duration-300 bg-transparent border-none ${section == 2 ? 'text-[#ECF3FF] bottom-line' : 'text-[#8690A0]'} `}>
                    Participants
                </button>

                <button onClick={() => setSection(3)} className={` transition-all  duration-300 bg-transparent border-none ${section == 3 ? 'text-[#ECF3FF] bottom-line' : 'text-[#8690A0]'} `}>
                    Team/Dev Wallets
                </button>

                <div className={`absolute transition-all  duration-300   -bottom-0  w-[80px] sm:w-[107px] h-[4px] bg-[#376EFF] ${section == 1 && 'left-[0%] sm:left-[7%] md:left-[12%]'} ${section == 2 && 'left-[37%] sm:left-[37%] md:left-[41%]'} ${section == 3 && 'left-[73%] sm:left-[66%] md-left-[69%]'}`}>

                </div>
            </div>

            <div className="pl-[28px] lg:pl-[90px] xl:pl-[111px] pr-[28px] pt-[24px] sm:pt-[40px] md:pt-[71px]" >
                {
                    section == 1 &&
                    <div className={`text-[16px] font-gilroy600 tracking-tight text-[#8690A0]  transition-all duration-300  ${section == 1 ? 'opacity-100' : 'opacity-0'}`}>
                        <p className="mb-[25px]">
                            <span className=' leading-[12px] gradient-text pr-[8px]'>1. </span>
                            <span className="font-gilroy700 text-[#ECF3FF]">Create Your Raffle: </span>
                            Easily craft your raffle on our user-friendly dApp.
                        </p>

                        <p className="mb-[25px]">
                            <span className=' leading-[12px] gradient-text pr-[8px]'>2. </span>
                            <span className="font-gilroy700 text-[#ECF3FF]">  Customization at Your Fingertips: </span>
                            Specify the raffle name, blockchain, and token address of your project.
                        </p>

                        <p className="mb-[25px]">
                            <span className=' leading-[12px] gradient-text pr-[8px]'>3. </span>
                            <span className="font-gilroy700 text-[#ECF3FF]">Set the Terms: </span>
                            Decide the number of tokens required for participation, duration of the raffle, and the distribution percentages for the winners, Team/Dev Wallet, and burn wallet.
                        </p>

                        <p className="mb-[25px]">
                            <span className=' leading-[12px] gradient-text pr-[8px]'>4. </span>
                            <span className="font-gilroy700 text-[#ECF3FF]">Get Your Unique URL: </span>
                            Once created, receive a unique URL to share with your community, making participation a breez
                        </p>

                    </div>
                }

                {
                    section == 2 &&
                    <div className={`text-[16px] font-gilroy600 tracking-tight text-[#8690A0] transition-all duration-300  ${section == 2 ? 'opacity-100' : 'opacity-0'}`} >
                        <p className="mb-[25px]">
                            <span className=' leading-[12px] gradient-text pr-[8px]'>1. </span>
                            <span className="font-gilroy700 text-[#ECF3FF]">Create Your Raffle: </span>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, esse?
                        </p>

                        <p className="mb-[25px]">
                            <span className=' leading-[12px] gradient-text pr-[8px]'>2. </span>
                            <span className="font-gilroy700 text-[#ECF3FF]">  Customization at Your Fingertips: </span>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, esse?
                        </p>

                        <p className="mb-[25px]">
                            <span className=' leading-[12px] gradient-text pr-[8px]'>3. </span>
                            <span className="font-gilroy700 text-[#ECF3FF]">Set the Terms: </span>
                            Decide the number of tokens required for participation, duration of the raffle, and the distribution percentages for the winners, Team/Dev Wallet, and burn wallet.
                        </p>

                        <p className="mb-[25px]">
                            <span className=' leading-[12px] gradient-text pr-[8px]'>4. </span>
                            <span className="font-gilroy700 text-[#ECF3FF]">Get Your Unique URL: </span>
                            Once created, receive a unique URL to share with your community, making participation a breez
                        </p>

                    </div>
                }

                {
                    section == 3 &&
                    <div className={`text-[16px] font-gilroy600 tracking-tight text-[#8690A0] transition-all duration-500  ${section == 3 ? 'opacity-100 ' : 'opacity-0'}`} >
                        <p className="mb-[25px]">
                            <span className=' leading-[12px] gradient-text pr-[8px]'>1. </span>
                            <span className="font-gilroy700 text-[#ECF3FF]">Create Your Raffle: </span>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, esse?
                        </p>

                        <p className="mb-[25px]">
                            <span className=' leading-[12px] gradient-text pr-[8px]'>2. </span>
                            <span className="font-gilroy700 text-[#ECF3FF]">  Customization at Your Fingertips: </span>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, esse?
                        </p>

                        <p className="mb-[25px]">
                            <span className=' leading-[12px] gradient-text pr-[8px]'>3. </span>
                            <span className="font-gilroy700 text-[#ECF3FF]">Set the Terms: </span>
                            Decide the number of tokens required for participation, duration of the raffle, and the distribution percentages for the winners, Team/Dev Wallet, and burn wallet.
                        </p>

                        <p className="mb-[25px]">
                            <span className=' leading-[12px] gradient-text pr-[8px]'>4. </span>
                            <span className="font-gilroy700 text-[#ECF3FF]">Get Your Unique URL: </span>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, esse?
                        </p>

                    </div>
                }

            </div>


        </div>
        )
}

export default SectionToggler