"use client";
import Image from "next/image";
import React, { useState } from "react";
const AboutUs = () => {
  return (
    <>
      <div className="w-[100%] sm:flex-row xs:flex-col flex gap-3 p-2 ">
        <div className="sm:w-[50%] xs:w-[100%] flex items-center">
          <div className="w-[98%] ">
            <div className="w-full">
              <h1 className="z-10 xs:text-center md:text-center lg:text-start  tracking-normal xl:tracking-wide mb-[30px] sm:mb-[73px] leading-[90px] text-[45px] sm:text-[60px] lg:text-[76px] xl:text-[86px] font-gilroy900 text-transparent bg-clip-text bg-gradient-to-r from-[#ECF3FF] to-[rgba(236, 243, 255, 0.26)]">
                About Us
              </h1>
            </div>
            <div>
              <p className="  text-[#ECF3FF] text-[16px] md:text-[24px] font-gilroy400 leading-[30px] mb-[16px]">
                Welcome to CoinRaffle.io – <br />
                Where Token Utility Meets Community Thrive!
              </p>
            </div>
            <div>
              <p className="  text-[#8690A0] text-[12px] md:text-[16px] font-gilroy400 leading-[30px] mb-[16px] text-justify">
                At CoinRaffle.io, born out of the collective vision to redefine
                community engagement, add real-world utility to tokens, and
                automate token burning, CoinRaffle.io is your gateway to a new
                era in the crypto space.
              </p>
            </div>
          </div>
        </div>
        <div className="sm:w-[50%] xs:w-[100%]">
          <div className="w-[100%] h-[100%]">
            {/* TOP IMAGE FOR THE DIV */}
            <Image
              src="/assets/images/about_bitcoin1.png"
              width={1000}
              height={1000}
              style={{ width: "100%", height: "100%" }}
              alt=""
            />
          </div>
        </div>
      </div>
    </>
  );
};
export default AboutUs;
{
  /* <div className="h-[100%] w-[100%] flex md:flex-row lg:flex-row xs:flex-col gap-1 justify-start ">
<div className="w-[50%] sm:w-[100%] xs:w-[100%] px-4 sm:pl-0 pt-[70px] sm:pt-[152px]  2xl:w-full mb-[60px] lg:mb-[273px]">
  <h1 className="z-10 text-center lg:text-start  tracking-normal xl:tracking-wide mb-[30px] sm:mb-[73px] leading-[90px] text-[45px] sm:text-[60px] lg:text-[76px] xl:text-[86px] font-gilroy900 text-transparent bg-clip-text bg-gradient-to-r from-[#ECF3FF] to-[rgba(236, 243, 255, 0.26)]">
    About Us
  </h1>
  <div className="mt-[20px] sm:mt-[73px] flex flex-col  lg:flex-row justify-between  gap-x-[40px] 2xl:gap-x-0">
    <div className="  mb-[150px] sm:mb-[200px] lg:mb-0 ">
      <div >
        <p className="  text-[#ECF3FF] text-[16px] md:text-[24px] font-gilroy400 leading-[30px] mb-[16px]">
          Welcome to CoinRaffle.io – <br />
          Where Token Utility Meets Community Thrive!
        </p>
      </div>
      <div >
        <p className="  text-[#8690A0] text-[12px] md:text-[16px] font-gilroy400 leading-[30px] mb-[16px]">
          At CoinRaffle.io, born out of the collective vision to
          redefine community engagement, add real-world utility to
          tokens, and automate token burning, CoinRaffle.io is your
          gateway to a new era in the crypto space.
        </p>
      </div>
    </div>
    </div>
</div>
<div className="w-[50%] sm:w-[100%] xs:w-[100%]">
    <div className="w-[100%] lg:min-w-[800px] md:min-w-[600px] sm:min-w-[500px] h-[100%] lg:min-h-[650px] md:min-h-[650px] sm:min-h-[450px] relative ">
      <Image    src="/assets/images/about_bitcoin1.png"
       width={1000}
       height={1000}
       style={{width:"100%",height:"100%"}}
        alt=""/>
    </div>
    </div>
  </div> */
}
