"use client";
import Image from "next/image";
import React, { useState } from "react";
const Commitment = () => {
  return (
    <>
      <div className="pb-[50px] pt-[100px] p-2">
        <p className="text-[#ECF3FF]  z-10 text-center lg:text-start text-[36px] sm:text-[50px] lg:text-[56px] xl:text-[50px] font-gilroy900  ">
          Our Commitment
        </p>
        <div className="flex md:flex-row xs:flex-col gap-4 pt-6 justify-between relative">
          <div className="w-[100%] max-w-[420px] h-[fit-content]  rounded-[50px] bg-gradient-to-r from-[#3C447D] to-[#2E5FE1] bg-opacity-100 p-[40px] ">
            <div className="flex gap-5 items-start">
              <div className="w-[100%] max-w-[30px] h-[100%] pt-2">
                {/* TOP IMAGE FOR THE DIV */}
                <Image
                  src="/assets/images/tranperancy.png"
                  width={1000}
                  height={1000}
                  style={{ width: "100%", height: "100%" }}
                  alt=""
                />
              </div>

              <h1 className="text-[#ECF3FF] font-gilroy800  text-[25px] xl:text-[30px]">
                Transparency
              </h1>
            </div>
            <p className="text-[#8690A0] text-[12px] md:text-[16px] font-gilroy400 leading-[30px] mb-[16px]">
              We are committed to transparency in every aspect of our platform.
              From Smart Contract audits to the execution of weekly buybacks,
              trust and openness define our operations.
            </p>
          </div>
          <div className="md:pt-12 xs:pt-0">
            <div className="relative w-[100%] max-w-[420px] rounded-[50px] bg-gradient-to-r from-[#3C447D] to-[#2E5FE1] bg-opacity-100 p-[40px] ">
              <div className="flex gap-5 items-start">
                <div className="w-[100%] max-w-[30px] h-[100%] pt-2">
                  {/* TOP IMAGE FOR THE DIV */}
                  <Image
                    src="/assets/images/security.png"
                    width={1000}
                    height={1000}
                    style={{ width: "100%", height: "100%" }}
                    alt=""
                  />
                </div>

                <h1 className="text-[#ECF3FF] font-gilroy800  text-[25px] xl:text-[30px]">
                  Security
                </h1>
              </div>
              <p className="text-[#8690A0] text-[12px] md:text-[16px] font-gilroy400 leading-[30px] mb-[16px]">
                Security is non-negotiable. Our non-custodial architecture
                ensures that your funds are secure, and our 24/7 Telegram
                Technical Support Chat is here to assist.
              </p>
              <div className="absolute md:right-[-280px] sm:right-[-190px] xs:right-[-25%]  xs:top-[124px]  ">
              <div className="w-[100%] md:w-[80%] sm:w-[80%] xs:w-[90%] h-[100%] ">
                <Image
                  src="/assets/images/bitcoin.png"
                  width={400}
                  height={400}
                  style={{ width: "100%", height: "100%" }}
                  alt=""
                />
              </div>
            </div>
            </div>
          </div>
          <div className="w-[100%] max-w-[420px] h-[fit-content] rounded-[50px] bg-gradient-to-r from-[#3C447D] to-[#2E5FE1] bg-opacity-100 p-[40px] ">
            <div className="flex gap-5 items-start">
              <div className="w-[100%] max-w-[30px] h-[100%] pt-2">
                {/* TOP IMAGE FOR THE DIV */}
                <Image
                  src="/assets/images/comunity.png"
                  width={1000}
                  height={1000}
                  style={{ width: "100%", height: "100%" }}
                  alt=""
                />
              </div>

              <h1 className="text-[#ECF3FF] font-gilroy800  text-[25px] xl:text-[30px]">
                Community Growth
              </h1>
            </div>
            <p className="text-[#8690A0] text-[12px] md:text-[16px] font-gilroy400 leading-[30px] mb-[16px]">
              CoinRaffle.io is more than a platform; it's a community builder.
              Join us on a journey where your community growth meets practical
              token utility.
            </p>
          </div>
        </div>
      </div>
    </>
  );
};
export default Commitment;
