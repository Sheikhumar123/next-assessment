'use client'
import { useState } from "react";
import { FaBars } from "react-icons/fa6";
import { IoClose } from "react-icons/io5";
import { FaTwitter } from "react-icons/fa";
import { FaTelegramPlane } from "react-icons/fa";
import { RiDiscordFill } from "react-icons/ri";
const Navbar = () => {
    const [isOpened, setIsOpened] = useState(false)
    return (
        <div className="flex  flex-row justify-between items-center px-4 sm:px-0  pt-[30px]">
            <div className="flex items-start  h-full cursor-pointer ">
                <img src="/assets/images/logo.png" alt="" width={154} height={24} />
            </div>

            <div className="hidden lg:flex flex-row justify-between gap-x-[48px]">
                <div className="text-[13px] text-[#8690A0] font-gilroy600  gap-x-[60px] flex flex-row justify-between items-center">
                    <a href="/creator" className="cursor-pointer hover:text-[#c5cbd4] hover:scale-[1.2] transition duration-200">Creator</a>
                    <a href="/about" className="cursor-pointer hover:text-[#c5cbd4] hover:scale-[1.2] transition duration-200">About</a>
                    <a href="#" className="cursor-pointer hover:text-[#c5cbd4] hover:scale-[1.2] transition duration-200">Player</a>
                    <a href="#" className="cursor-pointer hover:text-[#c5cbd4] hover:scale-[1.2] transition duration-200">FAQ</a>
                    <button
                        className="button cursor-pointer bg-[#1B2846] hover:bg-[#23345c] transition-all duration-800 hover:opacity-95 w-[152px] h-[56px] text-[16px] rounded-[26px] border-[2px] border-[#5B88FF] font-gilroy800 text-[#ECF3FF]">
                        Connect Wallet
                    </button>
                </div>


                <div className="flex flex-row items-center justify-between gap-x-5 ">
                    <span className="hover:scale-[1.2] text-[#8690A0] transition duration-300 cursor-pointer text-[22px]"> <FaTwitter /></span>
                    <span className="hover:scale-[1.2] text-[#8690A0] transition duration-300 cursor-pointer text-[23px]"> <FaTelegramPlane /></span>
                    <span className="hover:scale-[1.2] text-[#8690A0] transition duration-300 cursor-pointer text-[23px]"> <RiDiscordFill /></span>
                </div>
            </div>

            <button className="text-[#8690A0] text-[30px] cursor-pointer block lg:hidden" onClick={() => setIsOpened(true)}>
                <FaBars />
            </button>

            <div className={`fixed w-[100%] sm:w-[400px] top-0 h-[100vh] bg-[#1B2846] z-50 transition-all duration-700 ${isOpened ? 'right-0' : '-right-[100%]'}`}>

                <button className="absolute left-5 top-5 text-[#8690A0] text-[40px]" onClick={() => setIsOpened(false)}>
                    <IoClose />
                </button>


                <div className="flex flex-col justify-between gap-y-[48px] mt-24">
                    <div className="text-[18px] text-[#8690A0] gap-y-[30px] flex flex-col justify-between items-start pl-20">
                        <button className=" bg-[#1B2846] w-[152px] h-[56px] text-[16px] rounded-[30px] border-[2px] border-[#5B88FF] font-gilroy800 text-[#ECF3FF]">Connect Wallet</button>
                        <a href="/creator">Creator</a>
                        <a href="#">About</a>
                        <a href="#">Player</a>
                        <a href="#">FAQ</a>
                    </div>


                    <div className="flex flex-row justify-center gap-x-5 absolute bottom-[10%] left-[40%]  ">
                    <span className="hover:scale-[1.2] text-[#8690A0] transition duration-300 cursor-pointer text-[22px]"> <FaTwitter /></span>
                    <span className="hover:scale-[1.2] text-[#8690A0] transition duration-300 cursor-pointer text-[23px]"> <FaTelegramPlane /></span>
                    <span className="hover:scale-[1.2] text-[#8690A0] transition duration-300 cursor-pointer text-[23px]"> <RiDiscordFill /></span>
                    </div>
                </div>

            </div>

            {/* BORDER BOTTOM */}
            <div className="h-[1px] w-[2220px] absolute bg-[#231F37] hidden 2xl:block left-[-350px] bottom-[116px]">
            </div>
        </div>
    )
}

export default Navbar