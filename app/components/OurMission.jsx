"use client";
import Image from "next/image";
import React, { useState } from "react";
const OurMission = () => {
  return (
    <>
      <div className="w-[100%] sm:flex-row xs:flex-col-reverse flex gap-3 p-2">
        <div className="sm:w-[50%] xs:w-[100%]">
          <div className="w-[100%] h-[100%]">
            {/* TOP IMAGE FOR THE DIV */}
            <Image
              src="/assets/images/bitcoin_trolley.png"
              width={1000}
              height={1000}
              style={{ width: "100%", height: "100%" }}
              alt=""
            />
          </div>
        </div>
        <div className="sm:w-[50%] xs:w-[100%] flex items-center">
          <div className="w-[98%] ">
            <div className="w-full">
              <h1 className=" text-[#ECF3FF]  z-10 text-center lg:text-start  tracking-normal xl:tracking-wide md:mb-[30px] sm:mb-[10px] leading-[90px] text-[36px] sm:text-[50px] lg:text-[56px] xl:text-[50px] font-gilroy900  ">
                Our Mission
              </h1>
            </div>
            <div className="w-full">
              <p className="  text-[#8690A0] text-[12px] md:text-[16px] font-gilroy400 leading-[30px] mb-[16px]">
                Empowering crypto projects to grow exponentially and providing
                crypto enthusiasts with an innovative, transparent, and secure
                platform are at the core of our mission. We believe in the
                transformative power of community and the incredible potential
                that lies within each crypto project.
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default OurMission;
// {/* <div className="px-4 sm:pl-0  sm:pt-[152px] w-[100%]  2xl:w-full mb-[60px] lg:mb-[273px]">
// <div className="mt-[20px] sm:mt-[73px] flex flex-col  lg:flex-row justify-between w-full gap-x-[40px] 2xl:gap-x-0">
//   <div className="w-[98%] sm:w-[560px]  rounded-[50px]  relative px-[10px] lg:px-[20px] xl:pl-[60px] xl:pr-[50px] pt-[60px] sm:pt-[113px]">
//     {/* TOP IMAGE FOR THE DIV */}
//     <img
//       src="/assets/images/bitcoin_trolley.png"
//       className="absolute -top-[33%] sm:-top-[45.2%] -left-[10%] w-[420px] sm:w-auto h-[420px] sm:h-auto"
//       alt=""
//     />
//   </div>
//   <div className="w-[98%] sm:w-[500px] mb-[150px] sm:mb-[200px] lg:mb-0 ">
//     <div className="w-full">
//       <h1 className=" text-[#ECF3FF]  z-10 text-center lg:text-start  tracking-normal xl:tracking-wide mb-[30px] sm:mb-[73px] leading-[90px] text-[36px] sm:text-[50px] lg:text-[56px] xl:text-[50px] font-gilroy900  ">
//         Our Mission
//       </h1>
//     </div>
//     <div className="w-full">
//       <p className="  text-[#8690A0] text-[12px] md:text-[16px] font-gilroy400 leading-[30px] mb-[16px]">
//         Empowering crypto projects to grow exponentially and
//         providing crypto enthusiasts with an innovative,
//         transparent, and secure platform are at the core of our
//         mission. We believe in the transformative power of community
//         and the incredible potential that lies within each crypto
//         project.
//       </p>
//     </div>
//   </div>
// </div>
// </div> */}
