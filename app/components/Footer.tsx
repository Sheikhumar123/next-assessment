import { FaTwitter } from "react-icons/fa";
import { FaTelegramPlane } from "react-icons/fa";
import { RiDiscordFill } from "react-icons/ri";
const Footer = () => {
    return (
        <div className="flex px-4 sm:px-0 flex-row justify-between items-center h-[70px] sm:h-[116px]  border-[#231F37] border-t-[1px] 2xl:border-t-[0px]">

            <div className="items-center flex text-[#8690A0] text-[13px] cursor-pointer hover:text-[#c5cbd4] hover:scale-[1.2] transition duration-200">
                Copyright
            </div>

            <div className="text-[13px] text-[#8690A0] gap-[20px] hidden sm:gap-x-[60px] sm:flex flex-row justify-between items-center">
                <a href="#" className="cursor-pointer hover:text-[#c5cbd4] hover:scale-[1.2] transition duration-200">Creator</a>
                <a href="#" className="cursor-pointer hover:text-[#c5cbd4] hover:scale-[1.2] transition duration-200">About</a>
                <a href="#" className="cursor-pointer hover:text-[#c5cbd4] hover:scale-[1.2] transition duration-200">Player</a>
                <a href="#" className="cursor-pointer hover:text-[#c5cbd4] hover:scale-[1.2] transition duration-200">FAQ</a>
            </div>


            <div className="flex flex-row justify-between gap-x-5 ">
                 <span className="hover:scale-[1.2] text-[#8690A0] transition duration-300 cursor-pointer text-[22px]"> <FaTwitter /></span>
                    <span className="hover:scale-[1.2] text-[#8690A0] transition duration-300 cursor-pointer text-[23px]"> <FaTelegramPlane /></span>
                    <span className="hover:scale-[1.2] text-[#8690A0] transition duration-300 cursor-pointer text-[23px]"> <RiDiscordFill /></span>
            </div>


        </div>
    )
}

export default Footer
