import React, { useState } from 'react';

const TimeSelector = () => {
    const [hours, setHours] = useState('');
    const [minutes, setMinutes] = useState('');

    const formatWithLeadingZero = (value: number) => {
        return value < 10 ? `0${value}` : `${value}`;
    };

    const handleHoursChange = (event: { target: { value: string; }; }) => {
        const value = parseInt(event.target.value, 10);
        if (value >= 1 && value <= 12) {
            setHours(formatWithLeadingZero(value));
        }
    };

    const handleMinutesChange = (event: { target: { value: string; }; }) => {
        const value = parseInt(event.target.value, 10);
        if (value >= 0 && value <= 59) {
            setMinutes(formatWithLeadingZero(value));
        }
    };

    return (
        <div className='flex flex-row w-[117px] justify-center relative'>
            <input
                type="number"
                placeholder='00'
                className='bg-transparent outline-none w-[40%]'
                value={hours}
                onChange={handleHoursChange}
            />
            <span>:</span>
            <input
                type="number"
                placeholder='00'
                className='bg-transparent outline-none w-[40%]'
                value={minutes}
                onChange={handleMinutesChange}
            />
        </div>
    );
};

export default TimeSelector;
