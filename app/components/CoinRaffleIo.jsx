"use client";
import Image from "next/image";
import React, { useState } from "react";
const CoinRaffleIo = () => {
  return (
    <>
      <div className="pb-[100px] p-2">
        <p className="text-[#ECF3FF]  z-10 text-center lg:text-start text-[36px] sm:text-[50px] lg:text-[56px] xl:text-[50px] font-gilroy900  ">
          Why CoinRaffle.io?
        </p>
        <div className="flex md:flex-row xs:flex-col gap-4 pt-6 justify-between relative">
          <div className="relative w-[100%] max-w-[420px] h-[fit-content]  rounded-[50px] bg-gradient-to-r from-[#3C447D] to-[#2E5FE1] bg-opacity-100 p-[40px] ">
            <div className="flex gap-5 items-start">
              <div className="w-[100%] max-w-[30px] h-[100%] pt-2">
                {/* TOP IMAGE FOR THE DIV */}
                <Image
                  src="/assets/images/rev.png"
                  width={1000}
                  height={1000}
                  style={{ width: "100%", height: "100%" }}
                  alt=""
                />
              </div>

              <h1 className="text-[#ECF3FF] font-gilroy800  text-[25px] xl:text-[30px]">
                Revolutionizing Engagement
              </h1>
            </div>
            <p className="text-[#8690A0] text-[12px] md:text-[16px] font-gilroy400 leading-[30px] mb-[16px]">
              We offer a cutting-edge decentralized application (dApp) designed
              to seamlessly create and manage raffles, transforming community
              engagement dynamics.
            </p>
            <div className="absolute md:right-[-280px] sm:right-[-190px] xs:right-[-25%]  xs:top-[150px]  ">
              <div className="w-[100%] md:w-[80%] sm:w-[80%] xs:w-[90%] h-[100%] ">
                <Image
                  src="/assets/images/boost.png"
                  width={400}
                  height={400}
                  style={{ width: "100%", height: "100%" }}
                  alt=""
                />
              </div>
            </div>
          </div>
          <div className="md:pt-12 xs:pt-0 w-[100%] max-w-[420px]">
            <div className=" rounded-[50px] bg-gradient-to-r from-[#3C447D] to-[#2E5FE1] bg-opacity-100 p-[40px] ">
              <div className="flex gap-5 items-start">
                <div className="w-[100%] max-w-[30px] h-[100%] pt-2">
                  {/* TOP IMAGE FOR THE DIV */}
                  <Image
                    src="/assets/images/utility.png"
                    width={1000}
                    height={1000}
                    style={{ width: "100%", height: "100%" }}
                    alt=""
                  />
                </div>

                <h1 className="text-[#ECF3FF] font-gilroy800  text-[25px] xl:text-[30px]">
                  Boosting Token Utility
                </h1>
              </div>
              <p className="text-[#8690A0] text-[12px] md:text-[16px] font-gilroy400 leading-[30px] mb-[16px]">
                With CoinRaffle.io, crypto projects can unlock new dimensions of
                token utility. From customizable raffles to specifying terms,
                our platform enhances the value proposition of your token.
              </p>
            </div>
          </div>
          <div className="w-[100%] max-w-[420px] h-[fit-content]  rounded-[50px] bg-gradient-to-r from-[#3C447D] to-[#2E5FE1] bg-opacity-100 p-[40px] ">
            <div className="flex gap-5 items-start">
              <div className="w-[100%] max-w-[30px] h-[100%] pt-2">
                {/* TOP IMAGE FOR THE DIV */}
                <Image
                  src="/assets/images/burning.png"
                  width={1000}
                  height={1000}
                  style={{ width: "100%", height: "100%" }}
                  alt=""
                />
              </div>

              <h1 className="text-[#ECF3FF] font-gilroy800  text-[25px] xl:text-[30px]">
                Automated Token Burning
              </h1>
            </div>
            <p className="text-[#8690A0] text-[12px] md:text-[16px] font-gilroy400 leading-[30px] mb-[16px]">
              We understand the importance of sustainability. CoinRaffle.io
              automates token burning, contributing to the longevity and
              scarcity of your token.
            </p>
          </div>
        </div>
      </div>
    </>
  );
};
export default CoinRaffleIo;
