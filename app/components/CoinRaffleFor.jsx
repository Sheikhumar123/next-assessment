import Image from "next/image";
import { useState } from "react";

const CoinRaffleFor = () => {
  const [section, setSection] = useState(1);
  return (
    <>
      <p className="text-[#ECF3FF]  z-10 text-center lg:text-start text-[36px] sm:text-[50px] lg:text-[56px] xl:text-[50px] font-gilroy900  ">
        CoinRaffle for..
      </p>
      <div className="about-background2 w-[100%] md:max-w-[70%] xs:max-w-[100%]  rounded-[20px] relative top-[100px]  z-10">
        <div className=" w-[335px] sm:w-[450px] left-[10%]  md:w-[100%] absolute  -top-[60px] h-[60px] flex flex-row gap-[50px] text-[12px] sm:text-[14px] font-gilroy800 leading-[12px] ">
          <button
            onClick={() => setSection(1)}
            className={` transition-all  duration-300 bg-transparent border-none ${
              section == 1 ? "text-[#ECF3FF] bottom-line" : "text-[#8690A0]"
            } `}
          >
            Project Owners
          </button>

          <button
            onClick={() => setSection(2)}
            className={` transition-all  duration-300 bg-transparent border-none ${
              section == 2 ? "text-[#ECF3FF] bottom-line" : "text-[#8690A0]"
            } `}
          >
            Participants
          </button>

          <button
            onClick={() => setSection(3)}
            className={` transition-all  duration-300 bg-transparent border-none ${
              section == 3 ? "text-[#ECF3FF] bottom-line" : "text-[#8690A0]"
            } `}
          >
            Team/Dev Wallets
          </button>

          <div
            className={` absolute transition-all  duration-300   -bottom-0  w-[80px] sm:w-[107px] h-[4px] bg-[#376EFF] ${
              section == 1 && "left-[20%] xs:left-[0%] md:left-[0%]"
            } ${
              section == 2 &&
              "left-[40%] md:left-[14%] sm:left-[32%] xs:left-[34%]"
            } ${
              section == 3 &&
              "left-[38%] md:left-[28%] sm:left-[65%] xs:left-[78%] "
            }`}
          ></div>
        </div>

        <div className=" p-[50px] sm:p-[50px] md:p-[50px]">
          {section == 1 && (
            <div
              className={`md:w-[70%] xs:w-[100%] text-[16px] font-gilroy600 tracking-tight text-[#8690A0]  transition-all duration-300  ${
                section == 1 ? "opacity-100" : "opacity-0"
              }`}
            >
              <p className="font-gilroy700 ">
                Creating a raffle on our user-friendly dApp is a breeze. Specify
                the raffle details, set the terms, and receive a unique URL to
                share with your community. Watch your community engagement soar!
              </p>
            </div>
          )}

          {section == 2 && (
            <div
              className={`md:w-[70%] xs:w-[100%] text-[16px] font-gilroy600 tracking-tight text-[#8690A0]  transition-all duration-300  ${
                section == 2 ? "opacity-100" : "opacity-0"
              }`}
            >
              <p className="font-gilroy700 ">
                2 Creating a raffle on our user-friendly dApp is a breeze.
                Specify the raffle details, set the terms, and receive a unique
                URL to share with your community. Watch your community
                engagement soar!
              </p>
            </div>
          )}

          {section == 3 && (
            <div
              className={`md:w-[70%] xs:w-[100%] text-[16px] font-gilroy600 tracking-tight text-[#8690A0]  transition-all duration-300  ${
                section == 3 ? "opacity-100" : "opacity-0"
              }`}
            >
              <p className="font-gilroy700 ">
                3 Creating a raffle on our user-friendly dApp is a breeze.
                Specify the raffle details, set the terms, and receive a unique
                URL to share with your community. Watch your community
                engagement soar!
              </p>
            </div>
          )}
        </div>
        <div className="absolute md:right-[-15%]  xs:right-[-25%] md:top-[-125px] xs:top-[75px] ">
          <div className="md:w-[100%] xs:w-[300px] ">
            {/* TOP IMAGE FOR THE DIV */}
            <Image
              src="/assets/images/rafflefor.png"
              width={450}
              height={100}
              alt=""
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default CoinRaffleFor;
