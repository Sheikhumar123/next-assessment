"use client";
import Image from "next/image";
import React, { useState } from "react";
const WhoWeAre = () => {
  return (
    <>
      <div className="md:mb-0 xs:mb-[230px] p-2">
        <div className="w-[100%] sm:flex-row xs:flex-col flex gap-3 relative">
          <div className="sm:w-[50%] xs:w-[100%] md:pt-[30px] sm:pt-0">
            <div className="w-[100%] max-w-[500px] h-[100%]">
              {/* TOP IMAGE FOR THE DIV */}
              <Image
                src="/assets/images/whowe1.png"
                width={600}
                height={600}
                style={{ width: "100%", height: "100%" }}
                alt=""
              />
            </div>
          </div>
          <div className="sm:w-[50%] xs:w-[100%] flex items-center">
            <div className="w-[98%] ">
              <div className="w-full">
                <h1 className=" text-[#ECF3FF]  z-10 text-center lg:text-start  tracking-normal xl:tracking-wide md:mb-[30px] sm:mb-[10px] leading-[90px] text-[36px] sm:text-[50px] lg:text-[56px] xl:text-[50px] font-gilroy900  ">
                  Who We Are
                </h1>
              </div>
              <div className="w-full">
                <p className=" md:w-[76%] sm:w-[100%] text-[#8690A0] text-[12px] md:text-[16px] font-gilroy400 leading-[30px] mb-[16px]">
                  CoinRaffle.io is powered by a team passionate about the crypto
                  space. We understand the unique challenges and opportunities
                  that crypto projects face. Our commitment to innovation,
                  security, and community welfare sets us apart.
                </p>
              </div>
            </div>
          </div>
          <div className="absolute pt-6 pb-10 px-8 about-background3 rounded-[20px] md:w-[40%] xs:w-[90%] md:bottom-[90px] xs:bottom-[-170px] md:right-[32%] xs:right-[6%]">
            <p className="text-[#ECF3FF] text-[18px]">
              <span className="font-gilroy700 text-[20px] font-[600] text-[#fff]">
                Embark on this journey with us.{" "}
              </span>
              Unleash the potential of your project with CoinRaffle.io – Where
              Growth Meets Utility!
            </p>
            <div className="py-3 bottom-[-20px] px-4 rounded-[20px] bg-[#5B88FF] w-[fit-content] absolute xs:left-[30%]">
              <p className="text-[#fff]">Create Raffle Now</p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default WhoWeAre;
