'use client'
import React, { useState } from 'react'
import Footer from '../components/Footer'
import Navbar from '../components/Navbar'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css"
import TimeSelector from '../components/TimeSelector';
import { useRouter } from 'next/navigation';
const Creator = () => {
    const router = useRouter();
    const [isFocused, setIsFocused] = useState(false);
    const [timeMode, setTimeMode] = useState('AM')
    const handleFocus = () => {
        setIsFocused(true);
    };

    const handleBlur = () => {
        setIsFocused(false);
    };
    const [activeButton, setActiveButton] = useState(null);

    const handleButtonClick = (number: any) => {
        setActiveButton(number);
    };

    const [isTimeModeVisible, setIsTimeModeVisible] = useState(false);
    const [selectedDate, setSelectedDate] = useState(new Date());



    const handleDateChange = (date: any) => {
        setSelectedDate(date);
    };
    return (
        <main className="min-h-screen bg-[#171627] flex items-center justify-center overflow-hidden relative">
            <div className="w-[98%] sm:w-[90%] 2xl:w-[1440px] relative z-20">
                <Navbar />
                <div className="min-h-screen ">
                    <div className="px-4 sm:pl-0 pt-[70px] sm:pt-[152px] w-[100%]  2xl:w-full mb-[60px] lg:mb-[273px]">
                        <h1 className="z-10 text-center lg:text-start  tracking-normal xl:tracking-wide mb-[30px] sm:mb-[73px] leading-[90px] text-[45px] sm:text-[60px] lg:text-[76px] xl:text-[86px] font-gilroy900 text-transparent bg-clip-text bg-gradient-to-r from-[#ECF3FF] to-[rgba(236, 243, 255, 0.26)]">
                            Create Raffle
                        </h1>

                        {/* FORM CONTAINER */}
                        <div className='mt-[20px] sm:mt-[73px] flex flex-col items-center lg:flex-row justify-between w-full gap-x-[40px] 2xl:gap-x-0'>
                            <div className='w-[98%] sm:w-[500px] mb-[150px] sm:mb-[200px] lg:mb-0'>
                                <div className='flex flex-row gap-x-[4%] w-full'>
                                    <div className='flex flex-col justify-between items-start  w-[48%] mb-[30px]'>
                                        <p className="  text-[#9BA3B1] text-[12px] md:text-[14px] font-gilroy600 leading-[20px] mb-[16px]">
                                            Give a name to Your Raffel
                                        </p>
                                        <input
                                            type="text"
                                            className={`pl-[20px] text-[14px] border-[2px] rounded-[24px] h-[56px] w-full bg-transparent ${isFocused ? 'text-[#ECF3FF] border-[#373448]' : 'text-[#373448] border-[#373448]'
                                                } `}
                                            onFocus={handleFocus}
                                            onBlur={handleBlur}
                                        />
                                    </div>

                                    <div className='flex flex-col justify-between items-start  w-[48%] mb-[30px]'>
                                        <p className="  text-[#9BA3B1] text-[12px] md:text-[14px] font-gilroy600 leading-[20px] mb-[16px]">
                                            What is your token Contract Address
                                        </p>
                                        <input
                                            type="text"
                                            className={`pl-[20px] text-[14px] border-[2px] rounded-[24px] h-[56px] w-full bg-transparent ${isFocused ? 'text-[#ECF3FF] border-[#373448]' : 'text-[#373448] border-[#373448]'
                                                } `}
                                            onFocus={handleFocus}
                                            onBlur={handleBlur}
                                        />
                                    </div>
                                </div>


                                <div className='flex flex-col justify-between  w-full '>
                                    <p className="  text-[#9BA3B1] text-[12px] md:text-[14px] font-gilroy600 leading-[20px] mb-[16px]">
                                        Specify your token  blockchain
                                    </p>

                                    <div className='flex flex-row justify-between gap-x-4'>

                                        <div className={`${activeButton === 2 ? 'radial-border' : 'border-[2px]'}  border-[#373448]  rounded-[24px] w-[48%] h-[56px]  relative`}>
                                            <button
                                                onClick={() => handleButtonClick(2)}
                                                className={`${activeButton === 2 ? 'bg-[#2A2632]' : 'bg-transparent'} absolute top-[1px]  left-[1px] bg-[#2A2632] pl-[30px] w-[98%] sm:w-[99.2%] h-[54px] text-[14px] rounded-[24px]  font-gilroy700 text-[#ECF3FF] flex flex-row justify-start items-center `}>
                                                <img src="/assets/images/fantom.png" alt="" />
                                                <p className='pl-[16px]'>Fantom</p>
                                            </button>
                                        </div>

                                        <div className={`${activeButton === 1 ? 'radial-border' : 'border-[2px]'}  border-[#373448]  rounded-[24px] w-[48%] h-[56px]  relative`}>
                                            <button
                                                onClick={() => handleButtonClick(1)}
                                                className={`${activeButton === 1 ? 'bg-[#2A2632]' : 'bg-transparent'} absolute top-[1px]  left-[1px] bg-[#2A2632] pl-[30px] w-[98%] sm:w-[99.2%] h-[54px] text-[14px] rounded-[24px]  font-gilroy700 text-[#ECF3FF] flex flex-row justify-start items-center `}>
                                                <img src="/assets/images/bsc.png" alt="" />
                                                <p className='pl-[16px]'>BSC</p>
                                            </button>
                                        </div>


                                    </div>
                                </div>

                                <div className='flex flex-col justify-between  w-full mt-[30px]'>
                                    <p className="  text-[#9BA3B1] text-[12px] md:text-[14px] font-gilroy600 leading-[20px] mb-[16px]">
                                        Exact amount of tokens each participant must send to the raffle fund unique address, RaffleFund
                                        (to be generated once you hit Generate Raffle button)
                                    </p>
                                    <input
                                        type="text"
                                        className={`pl-[20px] text-[14px] border-[2px] rounded-[24px] h-[56px] w-[240px] bg-transparent ${isFocused ? 'text-[#ECF3FF] border-[#373448]' : 'text-[#373448] border-[#373448]'
                                            } `}
                                        onFocus={handleFocus}
                                        onBlur={handleBlur}
                                    />
                                </div>

                                <div className='flex flex-col justify-between  w-full mt-[30px]'>
                                    <p className="  text-[#9BA3B1] text-[12px] md:text-[14px] font-gilroy600 leading-[20px] mb-[16px]">
                                        Raffel End
                                    </p>
                                    <div className='flex flex-row gap-x-[5px] sm:gap-x-[20px]'>
                                        <div className='relative'>
                                            <DatePicker
                                                onChange={handleDateChange}
                                                selected={selectedDate}
                                                className={`bg-transparent relative z-10 px-[15px] sm:px-[20px] w-[98%] h-[56px] text-[12px] sm:text-[14px] rounded-[30px] border-[2px] border-[#373448] font-gilroy700 text-[#9BA3B1] flex flex-row justify-between items-center `}
                                            />
                                            <img src="/assets/images/calendar.png" className='absolute cursor-pointer top-[20px] right-[23px]' alt="" />

                                        </div>

                                        <div className={`relative bg-tranparent px-[15px] sm:px-[24px] w-[117px] h-[56px] text-[12px] sm:text-[14px] rounded-[30px] border-[2px] border-[#373448] font-gilroy700 text-[#9BA3B1] flex flex-row justify-between items-center `}>
                                            <TimeSelector />
                                            <img src="/assets/images/down.png" alt="" />
                                        </div>

                                        <div onClick={() => setIsTimeModeVisible(!isTimeModeVisible)} className={`relative bg-tranparent px-[15px] sm:px-[24px] w-[117px] h-[56px] text-[12px] sm:text-[14px] rounded-[30px] border-[2px] border-[#373448] font-gilroy700 text-[#9BA3B1] flex flex-row justify-between items-center `}>
                                            <p>{timeMode}</p>
                                            <img src="/assets/images/down.png" alt="" />

                                            <div className={`${isTimeModeVisible ? 'block' : 'hidden'} absolute  overflow-hidden flex flex-col h-[112px] top-[56px] w-full bg-[#171627] rounded-3xl border-[2px] border-[#373448] left-0 `}>
                                                <button onClick={() => setTimeMode('AM')} className='px-[15px] hover:bg-[#272c4e]  sm:px-[24px]  h-[56px] text-[12px] sm:text-[14px] border-b-[2px] border-[#373448]'>AM</button>
                                                <button onClick={() => setTimeMode('PM')} className='px-[15px] hover:bg-[#272c4e] sm:px-[24px]   h-[56px] text-[12px] sm:text-[14px] '>PM</button>
                                            </div>

                                        </div >
                                    </div>
                                </div>


                                <div className='w-full  justify-center xl:justify-start flex'>
                                    <button onClick={() => router.push("/creator/2345")} className="button  bg-[#5B88FF] hover:bg-[#23345c] transition-all duration-800 w-[182px] h-[56px] text-[14px] rounded-[30px] border-[2px] border-[#5B88FF] font-gilroy800 text-[#ECF3FF] mt-[30px] cursor-pointer">Generate Raffle</button>

                                </div>

                            </div>

                            <div className='w-[98%] sm:w-[580px] min-h-[576px] rounded-[50px] bg-gradient-to-b from-[#2E3560] to-[#181C32] relative px-[10px] lg:px-[20px] xl:pl-[60px] xl:pr-[50px] pt-[60px] sm:pt-[113px]'>
                                {/* TOP IMAGE FOR THE DIV */}
                                <img src="/assets/images/bg-top.png" className='absolute -top-[33%] sm:-top-[45.2%] -left-[10%] w-[320px] sm:w-auto h-[320px] sm:h-auto' alt="" />
                                <h1 className='text-[#ECF3FF] font-gilroy800  text-center xl:text-start text-[25px] xl:text-[30px] leading-[34px]'>Raffel End Distribution Rules</h1>

                                <div className='mt-[20px] sm:mt-[47px]'>
                                    <div className='text-[12px] sm:text-[14px] font-gilroy600 text-[#9BA3B1] leading-[26px] flex flex-row items-center flex-wrap gap-y-2 sm:gap-y-0  mb-[10px]'>
                                        <span className=' font-gilroy700 leading-[12px] gradient-text pr-[11px]'>1. </span> Winner will be able to claim
                                        <input type='number' className='ml-[10px] bg-transparent pl-3 mr-[5px] w-[68px] h-[41px] border-[#3B456D] rounded-[12px] sm:rounded-[16px] border-[2px]' />
                                        % of the raffle funds
                                    </div>
                                    <br />
                                    <div className='text-[12px] sm:text-[14px] font-gilroy600 text-[#9BA3B1] leading-[26px] flex flex-row items-center flex-wrap gap-y-2 sm:gap-y-0 mb-[10px]'>
                                        <span className=' font-gilroy700 leading-[12px] gradient-text pr-[11px]'>2. </span>
                                        Team wallet
                                        <input className='ml-[10px] bg-transparent pl-3 mr-[5px] w-[120px] sm:w-[168px] h-[34px] sm:h-[41px] border-[#3B456D] rounded-[12px] sm:rounded-[16px] border-[2px]' type='text' />
                                        will be able to claim
                                        <input className='ml-[10px] bg-transparent pl-3 mr-[5px] w-[50px] sm:w-[74px] h-[34px] sm:h-[41px] border-[#3B456D] rounded-[12px] sm:rounded-[16px] border-[2px]' />
                                        % of the raffle funds
                                    </div>

                                    <br />
                                    <div className='text-[12px] sm:text-[14px] font-gilroy600 text-[#9BA3B1] leading-[26px] flex flex-row items-center flex-wrap gap-y-2 sm:gap-y-0 w-full mb-[10px]'>
                                        <span className=' font-gilroy700 leading-[12px] gradient-text pr-[11px]'>3. </span>
                                        <input className=' mr-[5px] bg-transparent pl-3 w-[50px] sm:w-[74px] h-[34px] sm:h-[41px] border-[#3B456D] rounded-[12px] sm:rounded-[16px] border-[2px]' />
                                        % will go to this burn wallet
                                        <input className=' ml-[8px] w-[120px] sm:w-[168px] bg-transparent pl-3 h-[34px] sm:h-[41px] border-[#3B456D] rounded-[12px] sm:rounded-[16px] border-[2px] ' />
                                    </div>

                                    <br />
                                    <div className='text-[12px] sm:text-[14px] font-gilroy600 text-[#9BA3B1] leading-[26px] flex flex-row items-center flex-wrap gap-y-2 sm:gap-y-0 w-full mb-[10px]'>
                                        <span className=' font-gilroy700 leading-[12px] gradient-text pr-[11px]'>4. </span>
                                        <input className=' mr-[5px] w-[50px] sm:w-[74px] bg-transparent pl-3 h-[34px] sm:h-[41px] border-[#3B456D] rounded-[12px] sm:rounded-[16px] border-[2px]' />
                                        will go to CoinRaffle for processing Fee
                                    </div>


                                </div>
                            </div>
                        </div>


                    </div>
                </div>


                <Footer />


            </div>


            {/* IMAGES OF MESSH AND BG  */}
            <img src="/assets/images/bg.png" className=" hidden w-full h-[1080px] xl:block absolute left-0 -bottom-[190px] z-10" alt="" />
            <img src="/assets/images/bg-left-top.png" className=" hidden w-[733px] h-[831px]  xl:block absolute -right-10  top-0 z-10" alt="" />
            <img src="/assets/images/v2.png" className="hidden md:block absolute -bottom-[150px] right-0" alt="" />

            <div className="">
                <img src="/assets/images/v1.png" className="hidden md:block absolute top-[10.5%] left-[34%]" alt="" />
            </div>
        </main>
    )
}

export default Creator