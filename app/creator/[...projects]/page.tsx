'use client'
import React, { useState } from 'react'
import Footer from '../../components/Footer'
import Navbar from '../../components/Navbar'

const Creator = () => {
    const [isFocused, setIsFocused] = useState(false);
    const [isConnected, setIsConnected] = useState(false)

    const handleFocus = () => {
        setIsFocused(true);
    };

    const handleBlur = () => {
        setIsFocused(false);
    };

    const buttonCount = 40; // Set the number of buttons you want
    const buttons = [];
    for (let i = 0; i < buttonCount; i++) {
        buttons.push(
            <div key={i} className='radial-border2 rounded-[16px] w-[232px] h-[41px] relative'>
                <button className={`absolute bottom-[1px] left-[1px] px-[20px] w-[98%] sm:w-[230px] h-[39px] text-[14px] rounded-[16px] font-gilroy700 text-[#ECF3FF] flex flex-row justify-between items-center`}>
                    <p>0x123A04d5657...8afc</p>
                    <span>
                        <img src="/assets/images/shape.png" alt="" />
                    </span>
                </button>
            </div>
        );
    }
    return (
        <main className="min-h-screen bg-[#171627] flex items-center justify-center overflow-hidden relative">
            <div className="w-[98%] sm:w-[90%] 2xl:w-[1440px] relative z-20">
                <Navbar />
                <div className="min-h-screen ">
                    <div className="px-4 sm:pl-0 pt-[70px] sm:pt-[152px] w-[100%]  2xl:w-full  ">
                        <div className='flex flex-col justify-between items-center lg:items-start gap-y-[35px]'>
                            <h1 className="z-10 text-center lg:text-start  tracking-normal xl:tracking-wide  leading-[90px] text-[45px] sm:text-[60px] lg:text-[76px] xl:text-[86px] font-gilroy900 text-transparent bg-clip-text bg-gradient-to-r from-[#ECF3FF] to-[rgba(236, 243, 255, 0.26)]">
                                Ultimate Prize Raffle
                            </h1>

                            <div className=' rounded-[30px] gap-y-3   relative flex flex-col items-center sm:items-start sm:flex-row gap-x-5 '>
                                {/* CUSTOM FUNCTION TO MEET DESIGN PURRPOSES ONLY */}

                                <button
                                    onClick={()=>setIsConnected(true)}
                                    className={`${isConnected?'opacity-40  bg-[#252134] border-[#ECF3FF]' : 'bg-[#5B88FF] hover:bg-[#23345c] border-[#5B88FF] '}   button   transition-all duration-800 w-[200px] h-[56px] text-[14px] rounded-[24px] border-[2px] font-gilroy800 text-[#ECF3FF]  cursor-pointer`}>
                                    {isConnected ? 'Wallet Connected' : 'Connect Wallet'}
                                </button>

                                <div className='flex items-center sm:items-start flex-col'>
                                    <button className={`${isConnected ? 'participate-button border-none button2' : 'opacity-40  bg-[#252134] border-[#ECF3FF]'} button  hover:bg-[#23345c] transition-all duration-800 w-[200px] h-[56px] text-[14px] rounded-[24px] border-[2px]  font-gilroy800 text-[#ECF3FF]  cursor-pointer`}>
                                        Participate Now
                                    </button>

                                    <p className='text-[#9BA3B1] font-gilroy600 text-[12px] mt-[10px]'>Number of tokens for participation: 50 $KIRBY</p>
                                </div>
                            </div>

                        </div>

                        {/* FORM CONTAINER */}
                        <div className='mt-[20px] sm:mt-[45px] flex flex-col items-center xl:items-start xl:flex-row justify-between w-full gap-x-[40px] 2xl:gap-x-0'>
                            <div className='w-[98%] lg:w-[800px] 2xl:w-[500px]  mb-[90px] xl:mb-[200px] '>
                                <div className='flex flex-col gap-x-[4%] w-full'>

                                    <div className='flex flex-col gap-y-3 sm:flex-row justify-between items-center  w-full mb-[30px]'>
                                        <button className={`bg-tranparent  pl-[20px] pr-[24px] w-[348px] h-[56px] text-[14px] rounded-[30px] border-[2px] border-[#373448] font-gilroy700 text-[#9BA3B1] flex flex-row justify-between items-center `}>
                                            <p >https://rafflecoin.io/project12345</p>
                                            <span>
                                                <img src="/assets/images/shape.png" alt="" />
                                            </span>
                                        </button>

                                        <button className="button  bg-[#5B88FF] hover:bg-[#23345c] transition-all duration-800 w-[132px] h-[56px] text-[14px] rounded-[30px] border-[2px] border-[#5B88FF] font-gilroy800 text-[#ECF3FF]  cursor-pointer">Share</button>
                                    </div>

                                    <h1 className='text-[30px] text-center sm:text-start font-gilroy800 leading-[34px] text-[#ECF3FF] mb-[30px]'>Participants<span className='pl-[9px] font-gilroy600 text-[14px]'>(77/500)</span></h1>

                                    <div className='flex flex-row flex-wrap  justify-center h-[321px] gap-x-[24px] gap-y-[15px] overflow-y-scroll scrollbar'>
                                        {/* REPLACE THE FOLLOWING WITH AACTUAL BACKEND */}
                                        {buttons}
                                    </div>


                                    <div className='mt-[40px]'>
                                        <h1 className='text-[30px] font-gilroy900 leading-[34px] text-[#ECF3FF] mb-[22px]'>
                                            Total Fund Raised <br className='block sm:hidden' />
                                            <span className='text-[#FFE239]  sm:pl-[25px]'>1500 $KIRBY</span></h1>
                                        <hr className='border-t-[1px] border-white opacity-20' />

                                        <div className='flex flex-col items-center sm:items-start gap-y-[10px] mt-[25px]'>
                                            <div className='flex flex-row text-[#ECF3FF]'>
                                                <p className='w-[190px] sm:w-[288px] font-gilroy600 text-[16px] leading-[26px]'>Winner will claim</p>
                                                <span className='font-gilroy800 text-[20px] leading-[26px]'>500 $KIRBY</span>
                                            </div>

                                            <div className='flex flex-row text-[#ECF3FF]'>
                                                <p className='w-[190px] sm:w-[288px] font-gilroy600 text-[16px] leading-[26px]'>Burn wallet will receive</p>
                                                <span className='font-gilroy800 text-[20px] leading-[26px]'>700 $KIRBY</span>
                                            </div>

                                            <div className='flex flex-row text-[#ECF3FF]'>
                                                <p className='w-[190px] sm:w-[288px] font-gilroy600 text-[16px] leading-[26px]'>Team wallet will claim</p>
                                                <span className='font-gilroy800 text-[20px] leading-[26px]'>300 $KIRBY</span>
                                            </div>

                                        </div>

                                    </div>


                                </div>

                            </div>

                            <div className='w-[355px] h-[500px] sm:h-[700px] xl:h-[auto]  sm:w-[580px] rounded-[50px] relative  2xl:mt-0'>
                                {/* TOP IMAGE FOR THE DIV */}
                                <div className='time-bg relative w-full h-[280px] pt-[5px] sm:pt-[38px] px-[10px] sm:px-[60px] text-[#ECF3FF] z-20'>
                                    <h1 className='font-gilroy500 text-[20px] sm:text-[30px] tracking-[0.2em] uppercase leading-[34px] text-center '>Raffle ends in:</h1>

                                    <div className='flex flex-row mt-[5px] sm:mt-[37px] justify-center md:justify-start'>
                                        <div className='flex flex-col gap-y-[2px] sm:gap-y-[8px] text-[#ECF3FF]'>
                                            <h1 className='font-gilroy900 text-[70px] sm:text-[100px] leading-[90px] text-shadow2'>13
                                            </h1>
                                            <p className='text-[14px] font-gilroy700 tracking-[0.25em] md:leading-[34px] pl-[15%] '>HOURS</p>
                                        </div>

                                        <span className='text-[60px] px-[10px]'> : </span>

                                        <div className='flex flex-col gap-y-[2px] sm:gap-y-[8px] text-[#ECF3FF]'>
                                            <h1 className='font-gilroy900 text-[70px] sm:text-[100px] leading-[90px] text-shadow2'>45

                                            </h1>
                                            <p className='text-[14px] font-gilroy700 tracking-[0.25em] md:leading-[34px] pl-[15%]' >MINUTES</p>
                                        </div>

                                        <span className='text-[60px] px-[10px]'> : </span>

                                        <div className='flex flex-col gap-y-[2px] sm:gap-y-[8px]'>
                                            <h1 className='font-gilroy900 text-[70px] sm:text-[100px] leading-[90px] text3 text-shadow3'>14
                                            </h1>
                                            <p className='text-[14px] font-gilroy700 tracking-[0.25em] md:leading-[34px] pl-[15%] text-[#FFE239]'>SECONDS</p>
                                        </div>



                                    </div>

                                    <img src="/assets/images/img-small.png" className='absolute -bottom-[34px] sm:-bottom-[95px] -left-[119px]' alt="" />
                                </div>

                                <div className='w-[400px] h-[350px] sm:w-[649.2px] sm:h-[652px] relative bottom-[202px] sm:bottom-[149px] left-[6px] '>
                                    <img src="/assets/images/coins-bg.png" className=' ' alt="" />
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <Footer />

            </div>


            {/* IMAGES OF MESSH AND BG  */}
            <img src="/assets/images/bg.png" className=" hidden w-full h-[1080px] xl:block absolute left-0 -bottom-[190px] z-10" alt="" />
            <img src="/assets/images/bg-left-top.png" className=" hidden w-[733px] h-[831px]  xl:block absolute -right-10  top-0 z-10" alt="" />
            <img src="/assets/images/v2.png" className="hidden md:block absolute -bottom-[150px] right-0" alt="" />

            <div className="">
                <img src="/assets/images/v1.png" className="hidden md:block absolute top-[10.5%] left-[34%]" alt="" />

            </div>
        </main>
    )
}

export default Creator