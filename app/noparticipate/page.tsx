"use client";
import React, { useState } from "react";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import TimeSelector from "../components/TimeSelector";
import { useRouter } from "next/navigation";
import OurMission from '../components/OurMission'
import CoinRaffleIo from '../components/CoinRaffleIo'
import CoinRaffleFor from '../components/CoinRaffleFor'
import Commitment from '../components/Commitment'
import WhoWeAre from '../components/WhoWeAre'
import Image from "next/image";
const Creator = () => {
  const router = useRouter();
  const [isFocused, setIsFocused] = useState(false);
  const [timeMode, setTimeMode] = useState("AM");
  const handleFocus = () => {
    setIsFocused(true);
  };

  const handleBlur = () => {
    setIsFocused(false);
  };
  const [activeButton, setActiveButton] = useState(null);

  const handleButtonClick = (number: any) => {
    setActiveButton(number);
  };

  const [isTimeModeVisible, setIsTimeModeVisible] = useState(false);
  const [selectedDate, setSelectedDate] = useState(new Date());

  const handleDateChange = (date: any) => {
    setSelectedDate(date);
  };
  return (
    <main className="min-h-screen bg-[#171627] flex items-center justify-center overflow-hidden relative">
      <div className="w-[98%] sm:w-[90%] 2xl:w-[1440px] relative z-20">
        <Navbar />
        <div className=" w-[100%] flex gap-1 justify-center h-[100%] pb-[60px] ">
            <div className="flex flex-col  justify-center text-center items-center">
        <div className="w-[100%] max-w-[500px]  h-[100%]  ">
                {/* TOP IMAGE FOR THE DIV */}
                <Image    src="/assets/images/noprticipate.png"
                 width={1000}
                 height={1000}
                 style={{width:"100%",height:"100%"}}
                  alt=""/>
                {/* <img
                  src="/assets/images/about_bitcoin1.png"
                  className="absolute -top-[33%] sm:-top-[45.2%] -left-[10%] w-[420px] sm:w-auto h-[420px] sm:h-auto"
                  alt=""
                /> */}
              </div>
              <p className="text-[#ECF3FF]  z-10 text-center lg:text-start text-[36px] sm:text-[50px] lg:text-[56px] xl:text-[50px] font-gilroy900  ">
          Our Commitment
        </p>
        <div className="pt-[20px]">
        <div className="py-3 px-4 rounded-[20px] bg-[#5B88FF] w-[fit-content]">
                            <p className="text-[#fff]">Сreate a new Raffle</p>
                        </div>
                        </div>
        </div>
</div>
        <Footer />
      </div>

      {/* IMAGES OF MESSH AND BG  */}
      <img
        src="/assets/images/bg.png"
        className=" hidden w-full h-[1080px] xl:block absolute left-0 -bottom-[190px] z-10"
        alt=""
      />
      <img
        src="/assets/images/bg-left-top.png"
        className=" hidden w-[733px] h-[831px]  xl:block absolute -right-10  top-0 z-10"
        alt=""
      />
      <img
        src="/assets/images/v2.png"
        className="hidden md:block absolute -bottom-[150px] right-0"
        alt=""
      />

      <div className="">
        <img
          src="/assets/images/v1.png"
          className="hidden md:block absolute top-[10.5%] left-[34%]"
          alt=""
        />
      </div>
    </main>
  );
};

export default Creator;
// imgclass div
// px-[10px] lg:px-[20px] xl:pl-[60px] xl:pr-[50px] pt-[60px] sm:pt-[113px]