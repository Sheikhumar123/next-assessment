'use client'
import React, { useState } from 'react'
import Footer from '../components/Footer';
import Navbar from '../components/Navbar';
const Winner = () => {

    return (
        <main className="min-h-screen bg-[#171627] flex items-center justify-center overflow-hidden relative">
            <div className="w-[98%] sm:w-[90%] 2xl:w-[1440px] relative z-20">
                <Navbar />
                <div className="min-h-screen ">
                    <div className='flex flex-col justify-center items-center text-center pt-[73px]'>
                        <h1 className='text-[#ECF3FF] text-[50px] font-gilroy900'>Winner</h1>
                        <div className='radial-border2 rounded-[16px] w-[232px] h-[41px] relative mt-[31px]'>
                            <button className={`absolute bottom-[1px] left-[1px] px-[20px] w-[98%] sm:w-[230px] h-[39px] text-[14px] rounded-[16px] font-gilroy700 text-[#ECF3FF] flex flex-row justify-between items-center`}>
                                <p>0x123A04d5657...8afc</p>
                                <span>
                                    <img src="/assets/images/shape.png" alt="" />
                                </span>
                            </button>
                        </div>
                        <h1 className='text-[50px] sm:text-[70px] md:text-[100px] leading-[90px] font-gilroy900 gradient-text2 mt-[20px] mb-[0px] sm:mt-[50px] sm:mb-[60px]'>1500 $KIRBY</h1>

                        <div className='mt-[10px] md:mt-[40px] mb-[50px] md:mb-[110px]'>
                            <div className='flex flex-col items-center sm:items-start gap-y-[10px] mt-[25px]'>
                                <div className='flex flex-row text-[#ECF3FF]'>
                                    <p className='w-[190px] sm:w-[288px] font-gilroy600 text-[16px] leading-[26px] text-[#9BA3B1]'>Winner received:</p>
                                    <span className='font-gilroy800 text-[20px] leading-[26px]'>500 $KIRBY</span>
                                </div>

                                <div className='flex flex-row text-[#ECF3FF]'>
                                    <p className='w-[190px] sm:w-[288px] font-gilroy600 text-[16px] leading-[26px] text-[#9BA3B1]'>Burn wallet received 5%:</p>
                                    <span className='font-gilroy800 text-[20px] leading-[26px]'>700 $KIRBY</span>
                                </div>

                                <div className='flex flex-row text-[#ECF3FF]'>
                                    <p className='w-[190px] sm:w-[288px] font-gilroy600 text-[16px] leading-[26px] text-[#9BA3B1]'>Team wallet received 5%:</p>
                                    <span className='font-gilroy800 text-[20px] leading-[26px]'>300 $KIRBY</span>
                                </div>

                            </div>
                        </div>

                        <img src="/assets/images/winner.png" alt="" />
                    </div>
                </div>
                <Footer />

            </div>


            {/* IMAGES OF MESSH AND BG  */}
            <img src="/assets/images/bg.png" className=" hidden w-full h-[1080px] xl:block absolute left-0 -bottom-[190px] z-10" alt="" />
            <img src="/assets/images/bg-left-top.png" className=" hidden w-[733px] h-[831px]  xl:block absolute -right-10  top-0 z-10" alt="" />
            <img src="/assets/images/v2.png" className="hidden md:block absolute -bottom-[150px] right-0" alt="" />

            <div className="">
                <img src="/assets/images/v1.png" className="hidden md:block absolute top-[10.5%] left-[34%]" alt="" />

            </div>
        </main>
    )
}

export default Winner