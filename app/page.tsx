'use client'
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import SectionToggler from "./components/SectionToggler";

export default function Home() {
  return (
    <main className="min-h-screen bg-[#171627] flex items-center justify-center overflow-hidden ">
      <div className="w-[98%] sm:w-[90%] 2xl:w-[1440px] relative z-20">
        <Navbar />
        <div className="min-h-screen flex flex-col  ">

          {/* LANDING SECTION */}
          <div className="flex flex-col xl:flex-row justify-between items-center mb-[100px] sm:mb-[150px] xl:mb-[240px] ">
            <div className="px-4 sm:pl-0 pt-[125px] w-[100%] xl:w-[50%] 2xl:w-auto">
              <h1 className="home-text z-10 text-center sm:text-start tracking-normal xl:tracking-wide mb-[20px] sm:mb-[40px] leading-[60px] sm:leading-[90px] text-[50px] md:text-[82px] font-gilroy900 ">
                Grow Your
                <br />
                Crypto Community
              </h1>

              <p className="text-[24px] sm:text-[28px] text-[#ECF3FF] font-gilroy400 leading-[40px] mb-[20px] sm:mb-[43px] text-center sm:text-start">& Add Real-World Utility to Your Token!</p>

              <p className="mb-[40px] text-[#8690A0] text-center sm:text-start text-[12px] sm:text-[16px] font-gilroy600 leading-[30px]">NFT aggregators rankings and analysis. Find non-fungible token trading <br className="hidden sm:block" /> volumes, number of traders per N</p>

              <div className="flex flex-row justify-center sm:justify-start">
                <button className="button bg-[#496bc9] hover:bg-[#354e94] transition-all duration-800 w-[152px] h-[56px] text-[16px] rounded-[30px] border-[2px] border-[#5B88FF] font-gilroy800 text-[#ECF3FF] mr-4 cursor-pointer ">Launch Raffle</button>
                <button className="button cursor-pointer bg-[#8f76ce0d] hover:bg-[#b8aecf0d] transition-all duration-800  w-[56px] h-[56px] text-[16px] rounded-[22px] border-[2px] border-[#D36DFF] font-gilroy800 text-[#ECF3FF]">
                  <img className="ml-5" src="/assets/images/polygon.png" alt="" />
                </button>

              </div>
            </div>

            {/* IMAGE DIV */}
            <div className=" w-[100%] sm:w-[625px] relative 2xl:absolute h-[400px] sm:h-[574px] border-white border-1 2xl:right-16 px-3">
              <img src="/assets/images/coin.png" className="absolute w-full h-full object-contain z-30" alt="" />
            </div>
          </div>

          {/* ABOUT US */}
          <div className="relative w-[97%] 2xl:w-auto flex flex-col">
            <h1 className="mb-[60px] lg:mb-[40px] leading-[90px] text-[45px] md:text-[70px] font-gilroy900 text-transparent bg-clip-text bg-gradient-to-r from-[#ECF3FF] to-[rgba(236, 243, 255, 0.26)]">
              About Us
            </h1>
            <img src="/assets/images/about.png" className="absolute -right-[37%] -top-[100px] xl:-right-[10%] 2xl:right-[271px]  sm:-top-[218px] w-[405px] h-[250px] sm:w-[585px] sm:h-[361px] md:w-auto md:h-auto" alt="" />
            <div className="flex flex-col md:flex-row items-center gap-x-[10px] xl:gap-x-[47px] ">
              <div className="about-background relative z-20 rounded-[40px] w-[95%] md:w-[530px] min-h-[150px] md:min-h-[213px] py-[36px] pl-[15px] sm:pl-[25px] 2xl:pl-[53px] pr-[15px] sm:pr-[28px]">
                <p className="text-[20px]  xl:text-[24px] 2xl:text-[28px] font-gilroy400  leading-[30px] sm:leading-[44px] text-[#ECF3FF] 2xl:tracking-tight">
                  Are you a <span className="font-gilroy900">crypto project owner</span> or community lead looking to propel your project <span className="font-gilroy900">to new heights?</span>
                </p>
              </div>

              <div className="h-[3px] w-[76px] hidden md:block bg-gradient-to-r from-[#ECF3FF] to-[rgba(236, 243, 255, 0.26)]">

              </div>

              <div className=" rounded-[40px] w-[95%] md:w-[484px]  min-h-[150px] md:min-h-[213px]   items-end mt-10 sm:mt-0 sm:justify-end flex">
                <p className="text-center sm:text-start text-[16px] font-gilroy600 leading-[30px] text-[#8690A0] tracking-tight ">
                  Look no further! CoinRaffle.io brings you a cutting-edge decentralized application (dApp) designed to seamlessly create and manage raffles, transforming your community engagement and boosting your token's utility.                </p>
              </div>

            </div>
          </div>

          {/* HOW IT WORKS ? */}
          <div className="mt-[100px] md:mt-[170px]">

            <h1 className="mb-[20px] text-center lg:text-start md:mb-[40px] leading-[90px] text-[45px] md:text-[70px] font-gilroy900 text-transparent bg-clip-text bg-gradient-to-r from-[#ECF3FF] to-[rgba(236, 243, 255, 0.26)]">
              How it works?
            </h1>

            <div className=" flex flex-col lg:flex-row gap-y-16  justify-center items-center ">
              <div className="group w-[90%] sm:w-[80%] lg:w-[661px] h-[471px] rounded-[45px] overflow-hidden relative z-20" >
                <img src="/assets/images/video2.png" className="w-full h-full object-cover object-center" />
                <button className="button transition-all duration-700 bg-transparent group-hover:bg-[#1435B0] absolute w-[70px] pl-[20px] object-contain rounded-md h-[70px] top-[39%] left-[48%]" >
                  <img src="/assets/images/polygon2.png" className="h-[40px] w-[40px] " />
                </button>

              </div>

             <SectionToggler />
            </div>
          </div>

          {/* TRANSFORM */}
          <div className="relative flex flex-col items-center md:items-start md:flex-row mb-[242px] mt-[220px]" >
            <div className="  sm:mr-[40px] xl:mr-[80px] text-center md:text-start">
              <h1 className="mb-[20px] md:mb-[40px] sm:leading-[90px] text-[36px] sm:text-[70px] font-gilroy900 text-[#ACB1BF] sm:text-transparent bg-clip-text bg-gradient-to-r from-[#ECF3FF] to-[rgba(236, 243, 255, 0.26)]">
                Ready to <br className="hidden sm:block"/> Transform Your <br  className="hidden sm:block"/> Project?
              </h1>
              <button className="mt-[20px] sm:mt-[50px] button bg-[#496bc9] hover:bg-[#354e94] transition-all duration-800 w-[182px] h-[56px] text-[16px] rounded-[30px] font-gilroy800 text-[#ECF3FF] cursor-pointer ">Create Raffle Now</button>
            </div>

            <div className="about-background mt-[40px] relative z-20 rounded-[40px] w-[95%] sm:w-[446px] min-h-[250px] xl:h-[230px] py-[39px] pl-[48px] pr-[41px]">
              <p className="text-[16px] font-gilroy600 leading-[30px] text-[#ECF3FF] tracking-tight">
                At CoinRaffle.io, we are committed to transparency, security, and fostering thriving crypto communities. Join us on this journey where community growth meets practical token utility. Unleash the potential of your project with CoinRaffle.io today!
              </p>

              <div className="absolute top-[120px] xl:top-[50px] -right-[100px] xl:-right-[220px] w-[450px] h-[450px] md:w-auto md:h-auto">
                <img src="/assets/images/rocket.png " alt="" />
              </div>
            </div>

          </div>

        </div>


        <Footer />


      </div>

      <div className="">
        <img src="/assets/images/bg.png" className="back-image hidden w-full  xl:block absolute left-0 h-[1850px] xl:top-[550px] 2xl:top-[541px] z-10" alt="" />
        <img src="/assets/images/v1.png" className="hidden md:block absolute top-[19%] left-[34%]" alt="" />
        <img src="/assets/images/v2.png" className="hidden md:block absolute top-[152%] xl:top-[102%] right-10" alt="" />

      </div>
    </main>
  );
}
